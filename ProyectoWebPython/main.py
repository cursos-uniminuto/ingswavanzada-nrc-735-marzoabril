
# Ejemplo Web con Flask y Python

# importar el framework de Flask
from flask import Flask, render_template

# instanciar el framework para utilizarlo
app = Flask(__name__);

# generar una ruta o fichero a utilizar
@app.route("/")     # ruta inicial del proyecto
def index():
    # return "<br><br><br><h1>Ejemplo Web con el Framework Flask de Python</h1>";
    return render_template('index.html');

# Crear una nueva ruta
@app.route("/imagen")
def imagen ():
    # return '<img url = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Logotipo_de_la_Corporaci%C3%B3n_Universitaria_Minuto_de_Dios.svg/1200px-Logotipo_de_la_Corporaci%C3%B3n_Universitaria_Minuto_de_Dios.svg.png" alt="Img No Disponible" />';
    return render_template('imagen.html');

@app.route("/estudiantes")
def estudiantes():
    # return "<br><br><h3>Curso Ing. Sw. Avanzada</h3> <br><br> <h2>Uniminuto</h2> <br><br> <h1> Daniel Acosta </h1> <br><br> <h1> Deivy Castañeda </h1> <br><br> <h1> Jahir Saavedra </h1>"
    return render_template('estudiantes.html');

if __name__ == "__main__":
    app.run(debug = True);  # reinicia el servidor de forma automatizada al realizar cambios.

