
<h3>Command line instructions</h3>

You can also upload existing files from your computer using the instructions below.

<h4>Git global setup</h4>

git config --global user.name "Jahir Saavedra" <br>
git config --global user.email "jahir.saavedra@uniminuto.edu"

<h4>Create a new repository</h4>

git clone https://gitlab.com/cursos-uniminuto/ingswavanzada-nrc-735-marzoabril.git <br>
cd ingswavanzada-nrc-735-marzoabril <br>
git switch --create main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push --set-upstream origin main


<h4>Push an existing folder</h4>

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/cursos-uniminuto/ingswavanzada-nrc-735-marzoabril.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push --set-upstream origin main


<h4>Push an existing Git repository</h4>

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/cursos-uniminuto/ingswavanzada-nrc-735-marzoabril.git <br>
git push --set-upstream origin --all <br>
git push --set-upstream origin --tags




Integración con Jira [UH1] [SCRUM-2]
